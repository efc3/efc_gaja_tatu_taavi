# EFC_Gaja_Tatu_Taavi

GitLab Issues REST API Task for EFC

**Assignment**
```
Create yourself a free credentials and a public project in gitlab.com

With the test tool of your choosing, create tests for the CRUD  
(https://en.wikipedia.org/wiki/Create,_read,_update_and_delete)  
operations for Gitlab issues described in the REST documentation

Add to the test set couple test cases verifying some edge case with the CRUD operations  
(your discretion on what edge cases you cover)

Create simple pipeline in gitlab.com that runs the tests
```

## Framework of choice

The Framework for this task is chosen to be pytest, due its powerful fixture scope capabilities  
and its high extendability:

- [ ]  Pytest: https://docs.pytest.org/en/7.1.x/
- [ ]  Pytest Plugins: https://docs.pytest.org/en/latest/reference/plugin_list.html
- [ ]  In tests, python-gitlab library is used: https://python-gitlab.readthedocs.io/en/stable/

### Required libraries

Required libraries are chosen on the go and added to ''requirements.txt''  
With requirement.txt, IDEs automatically suggest needed libraries to be installed,  
and it can be given for gitlab runner pip as an argument.

## Task status

### DONE

- [ ] Local IDE/pytest environment: 0.5h
- [ ] GitLab Setup: 0.1h
- [ ] GitLab CI/CD, gitlab-ci.yml: 0.1h
- [ ] GitLab Runner configuration: 0.1h
- [ ] TDD Placeholders for Task Tests: 0.1h
- [ ] Randomize (pytest-randomly) test execution to make sure that tests can be run independently
- [ ] Implement Class/Function Scope Gitlab Fixtures in conftest.py
- [ ] Implement test setup in conftest.py
- [ ] Basic smoketests for POST, GET, DELETE of Issues.
- [ ] Automatic execution upon push
- [ ] Paraller execution of POST, DELETE and GET tests
- [ ] Total time used, approximately 3h 45mins
### TODO
- [ ] Add simple update tests
- [ ] Allure reporting, Implement own allure setup

### Known issues

- [ ] Allure history is not retrieved properly to form Historical test results  
  in Gitlab Pages. Needs a bit of investigation

## GitLab helps: Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/efc3/efc_gaja_tatu_taavi.git
git branch -M main
git push -uf origin main
```

## GitLab helps: Integrate with your tools

- [ ] [Set up project integrations](https://gitlab.com/efc3/efc_gaja_tatu_taavi/-/settings/integrations)

## GitLab helps: Collaborate with your team

- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)

## GitLab helps: Test and Deploy

Use the built-in continuous integration in GitLab.

- [ ] [Get started with GitLab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/index.html)
- [ ] [Analyze your code for known vulnerabilities with Static Application Security Testing(SAST)](https://docs.gitlab.com/ee/user/application_security/sast/)
- [ ] [Deploy to Kubernetes, Amazon EC2, or Amazon ECS using Auto Deploy](https://docs.gitlab.com/ee/topics/autodevops/requirements.html)
- [ ] [Use pull-based deployments for improved Kubernetes management](https://docs.gitlab.com/ee/user/clusters/agent/)
- [ ] [Set up protected environments](https://docs.gitlab.com/ee/ci/environments/protected_environments.html)

***

