import pytest
import gitlab
import os
import logging
from error_msgs import *
from conftest import *
# Basic logging conf is done in pytest.ini
log = logging.getLogger(__name__)


class BasicTest:
    """
        # Lets initialize class-level gitlab session
        # Then we pass it as subclass to test class
    """
    pass


class TestGetProjects(BasicTest):
    def test_get_project_id_by_correct_namespace(self):
        """
            In this test, we try to get project id with correct namespace
        :return:
        """
        namespace = "efc3/efc_gaja_tatu_taavi"
        project_id = get_project_id_by_name(namespace=namespace, private_token=EFC_ACCESS_TOKEN)
        log.info("Project ID " + str(project_id))

    def test_get_project_id_by_incorrect_namespace(self):
        """
            In this test, we try to get project id with incorrect namespace
            We catch error gracefully, though we could let python womit
            exception.
        :return:
        """

        try:
            namespace = "efsc3/efc_gaja_tatu_taavi"
            project_id = get_project_id_by_name(namespace=namespace, private_token=EFC_ACCESS_TOKEN)
            log.info("Project ID " + project_id)
            assert "Project ID found: %s with name %s even it should not" %(project_id, namespace)
        except Exception as e:
            err = e.args[0]
            log.info("Exception: %s" % str(e))
            assert err == PROJECT_NOT_FOUND


class TestGetIssues(BasicTest):
    def test_get_existing_issues_by_namespace_as_admin(self, admin_login):
        """
            In this test we check if there is at least 2 issues in
            two projects.
            We get all the issues authenticated by correct user
            and having two projects
        :return:
        """

        namespace = "efc3/efc_gaja_tatu_taavi"
        gls = admin_login
        project_id = get_project_id_by_name(namespace=namespace, private_token=EFC_ACCESS_TOKEN)
        issues = gls.issues.list(project_id=project_id)
        for issue in issues:
            # Lets just debug project id for now
            log.debug(issue.project_id)

        log.info("Count of issues should be greater than 0: " + str(len(issues)))

        assert 0 < int(len(issues))

    def test_get_all_existing_issues_as_admin(self, admin_login):
        """
            In this test we check if there is at least 1 issue in
            two projects.
            Check more comprehensive that just plainly getting
            all issues.
        :return:
        """

        # This is efc3/efc_gaja_tatu_taavi public project
        # In this scenario there 1 issue placeholder for project 1
        project_1 = get_project_id_by_name("efc3/efc_gaja_tatu_taavi")
        # In this scenario there 1 issue placeholder for project 2
        project_2 = get_project_id_by_name("efc3/efc_ksnodlehs")

        gls = admin_login
        issues = gls.issues.list()
        project_1_issues = 0
        project_2_issues = 0

        for issue in issues:
            if int(issue.project_id) == int(project_1):
                project_1_issues += 1
                log.debug(project_1_issues)
            elif int(issue.project_id) == int(project_2):
                project_2_issues += 1
                log.debug(project_2_issues)

            log.debug(str(issue))

        result = True

        log.info("Total count of issues: " + str(len(issues)))
        log.info("Total count of Project 1 issues: " + str(project_1_issues))
        log.info("Total count of Project 2 issues: " + str(project_2_issues))

        # In this scenario project 1 has one issue
        assert 1 <= project_1_issues
        # In this scenario project 2 has one issue
        assert 1 <= project_2_issues

    def test_get_issue_state_open_found(self, admin_login):
        """
            In this test, we get all state=open issues from logged in user
        :return:
        """
        # This is efc3/efc_gaja_tatu_taavi public project
        # In this scenario there 1 issue placeholder for project 1
        project_1 = get_project_id_by_name("efc3/efc_gaja_tatu_taavi")

        gls = admin_login
        # We should have one closed issue
        issues = gls.issues.list(state="closed")
        log.info("Issues with Closed state: " + str(len(issues)))
        assert 0 < int(len(issues))

    def test_get_issue_state_invalid_not_found(self, admin_login):
        """
            In this case, we check that correct error is found
            when no issues is found with specified state
        :return:
        """
        # This is efc3/efc_gaja_tatu_taavi public project
        # In this scenario there 1 issue placeholder for project 1
        project_1 = get_project_id_by_name("efc3/efc_gaja_tatu_taavi")

        gls = admin_login
        state = "XXX"
        # We should throw an exception of invalid state state issues
        try:
            issues = gls.issues.list(state=state)
            log.info("Issue state %s found even not expected " % state)
            assert "Issue state %s found even not expected "  % state
        except Exception as e:
            err = e.args[0]
            log.info("Exception: %s" % str(e))
            # Womitting more info from traceback. If we need, we can catch everything
            log.info("Error Code: {c}, Message, {m}".format(c=type(e).__name__, m=str(e)))
            assert err == ISSUES_STATE_NOT_FOUND
