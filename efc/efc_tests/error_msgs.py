ISSUES_STATE_NOT_FOUND = "state does not have a valid value"
PROJECT_NOT_FOUND = "404 Project Not Found"
ISSUES_MISSING_TITLE = "Missing attributes: title"
ISSUES_READONLY_CREATE_FAILURE = "insufficient_scope"