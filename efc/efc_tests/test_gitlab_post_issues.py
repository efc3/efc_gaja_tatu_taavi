import gitlab
import os
import logging
import time
from conftest import *
from error_msgs import *

# Basic logging conf is done in pytest.ini
log = logging.getLogger(__name__)


class TestPostIssues:
    def test_post_create_delete_valid_issue(self, admin_login):
        """
            In this test, we create and delete an issue
        :return:
        """
        namespace = "efc3/efc_gaja_tatu_taavi"

        gls = admin_login
        project_id = get_project_id_by_name(namespace=namespace)
        # Lets get correct project to work with
        project = gls.projects.get(namespace)
        ts = time.time()
        log.info("Creating issue: Title: %s - %s" % (namespace, ts))
        issue = project.issues.create({'title': '%s - %s'
                                                % (namespace, ts),
                                       'description': '%s - %s' % (namespace, ts)})
        issue_id = issue.get_id()
        log.info("Created issue: Title: %s" % issue.title)
        log.info(str(project))
        log.info(str(issue))
        log.info(str(issue_id))
        assert str(issue.title) == "%s - %s" % (namespace, ts)
        project.issues.delete(issue_id)

    def test_post_create_issue_missing_title(self, admin_login):
        """
            In this test we try to create an issue, without mandatory parameter title
        :return:
        """
        namespace = "efc3/efc_gaja_tatu_taavi"

        gls = admin_login
        project_id = get_project_id_by_name(namespace=namespace)
        # Lets get correct project to work with
        project = gls.projects.get(namespace)
        ts = time.time()
        log.info("Creating issue with invalid parameters: Title_XXX: %s - %s" % (namespace, ts))

        try:
            issue = project.issues.create({'title_XXX': '%s - %s'
                                           % (namespace, ts),
                                           'description': '%s - %s' % (namespace, ts)})
        except Exception as e:
            err = e.args[0]
            log.info("Exception: %s" % str(e))
            # Womitting more info from traceback. If we need, we can catch everything
            log.info("Error Code: {c}, Message, {m}".format(c=type(e).__name__, m=str(e)))
            assert err == ISSUES_MISSING_TITLE

    def test_test_post_create_issue_no_user_rights(self, readonly_login):
        namespace = "efc3/efc_gaja_tatu_taavi"

        # Credentials are readonly
        gls = readonly_login
        project_id = get_project_id_by_name(namespace=namespace)
        # Lets get correct project to work with
        project = gls.projects.get(namespace)
        ts = time.time()
        log.info("Creating issue with readonly parameters: Title: %s - %s" % (namespace, ts))

        try:
            issue = project.issues.create({'title': '%s - %s'
                                           % (namespace, ts),
                                           'description': '%s - %s' % (namespace, ts)})
            assert "Creating issue to Namespace: %s, Project ID: %s succeeded, even it should not!!" % (namespace, project_id)
        except Exception as e:
            err = e.args[0]
            log.info("Exception: %s" % str(e))
            # Womitting more info from traceback. If we need, we can catch everything
            log.info("Error Code: {c}, Message, {m}".format(c=type(e).__name__, m=str(e)))
            assert err == ISSUES_READONLY_CREATE_FAILURE

