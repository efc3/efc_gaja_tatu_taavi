import gitlab
import os
import logging
# Basic logging conf is done in pytest.ini
log = logging.getLogger(__name__)


class TestDeleteIssues:
    def test_delete_existing_issue(self):
        assert "TDD Expected Failure" == "TDD Successful"

    def test_delete_non_existing_issue(self):
        assert "TDD Expected Failure" == "TDD Successful"

    def test_delete_existing_issue_not_admin(self):
        assert "TDD Expected Failure" == "TDD Successful"
