import gitlab
import pytest
import os
import logging
from conftest import admin_login
log = logging.getLogger(__name__)
EFC_ACCESS_TOKEN = os.getenv('EFC_ACCESS_TOKEN')
EFC_ACCESS_TOKEN_READONLY = os.getenv('EFC_ACCESS_TOKEN_READONLY')


class TestEnvironment:
    def test_env_pytest(self):

        log.info("Lets print system environment variables for debugging purposes")
        for k, v in os.environ.items():
            log.debug("%s - %s" % (k, v))

    def test_gitlab_admin_access(self, admin_login):

        # log.info("Initializing Project admin access")
        gls = admin_login
        # Lets get the CI_PROJECT_ID and CI_PROJECT_PATH set by GitLab CI/CD automatically for Runner
        # In local development environment, these has to be set as user variable
        #
        project_id = os.getenv("CI_PROJECT_ID")
        project_path = os.getenv("CI_PROJECT_PATH")
        log.info("Project path: %s" % project_path)
        log.info("Project ID  : %s" % project_id)
        project = gls.projects.get(project_id)
        attrib = project.attributes
        log.info("Lets print project environment variables for debugging purposes")
        for k, v in attrib.items():
            log.debug("%s - %s" % (k, v))


