# content of conftest.py
# Import the 'modules' that are required for execution
import gitlab
import os
import pytest
import logging
log = logging.getLogger(__name__)
# Basic logging conf is done in pytest.ini

# Lets get the EFC Access Token set by pipeline variable
# In local development environment, it has to be set as user variable
# It is masked in the logs, so we can print all variables later
#
EFC_ACCESS_TOKEN = os.getenv('EFC_ACCESS_TOKEN')
EFC_ACCESS_TOKEN_READONLY = os.getenv('EFC_ACCESS_TOKEN_READONLY')


@pytest.fixture
def admin_login():
    """
        Admin login to gitlab
    """
    gls = None
    # Make connection to gitlab project
    #
    log.info("Initializing Project admin access")
    gls = gitlab.Gitlab('https://gitlab.com', private_token=EFC_ACCESS_TOKEN, api_version='4')
    gls.auth()
    return gls


@pytest.fixture
def login_admin_and_create_issue_fs():
    """
        Admin login to gitlab and create issue
    """
    gls = None
    # Make connection to gitlab project
    #
    log.info("Initializing Project admin access")
    gls = gitlab.Gitlab('https://gitlab.com', private_token=EFC_ACCESS_TOKEN, api_version='4')
    gls.auth()
    return gls


@pytest.fixture
def readonly_login():
    """
        Readonly login to gitlab
    """
    gls = None
    # Make connection to gitlab project
    #
    log.info("Initializing Project readonly access")
    gls = gitlab.Gitlab('https://gitlab.com', private_token=EFC_ACCESS_TOKEN_READONLY, api_version='4')
    gls.auth()
    return gls


def get_project_id_by_name(namespace="efc3/efc_gaja_tatu_taavi", private_token=EFC_ACCESS_TOKEN):
    """
        Get the project id by name
    """
    gls = None
    # Make connection to gitlab project
    #
    log.info("Initializing Project access")
    gls = gitlab.Gitlab('https://gitlab.com', private_token=private_token, api_version='4')
    gls.auth()
    project = gls.projects.get(namespace)
    log.info("Project ID %s for %s" % (project.get_id(), namespace))
    return project.get_id()
